##############################################
#########         PACOTES              #######
##############################################

# install.packages(c("tidyquant","tidyverse","Quandl","rprojroot",
#                    "mfGARCH", "BETS", "dygraphs", "ecoseries",
#                    "tbl2xts", "deflateBR"))

# devtools::install_github('wilsonfreitas/rbcb')

# Pacotes para coletar os dados
suppressMessages(require(tidyquant))
suppressMessages(require(Quandl))
suppressMessages(require(BETS))
suppressMessages(require(ecoseries))
suppressMessages(require(rbcb))

# Pacotes para manusear os dados
suppressMessages(require(tidyverse))
suppressMessages(require(tbl2xts))

# Pacote para facilitar o uso de arquivos do projeto
suppressMessages(require(rprojroot))

# Pacote para estimar o modelo GARCH-MIDAS
suppressMessages(require(mfGARCH))

# Pacote para gerar os gráficos e tabelas
suppressMessages(require(dygraphs))
suppressMessages(library(ggplot2))
suppressMessages(library(ggfortify))
suppressMessages(library(scales))
suppressMessages(library(ggpubr))
suppressMessages(library(ggthemes))
theme_set(theme_gdocs())
suppressMessages(library(expss))
suppressMessages(library(gridExtra))

# Pacote para deflacionar séries
suppressMessages(require(deflateBR))

##############################################
#######       PATH DEFAULT              ######
##############################################

# Garantir que o path para o projeto é definido automaticamente. 
# Isso nos garante que uma vez clonado o projeto ele pode ser 
# executado sem necessidade de configurações alternativas de path.

root = rprojroot::is_rstudio_project 
mydir = root$find_file()   # Encontra o diretório no qual o projeto atual está salvo

##############################################
######         QUANDL API KEY           ######
##############################################

quandl_api_key("NjGc22-41R7zD_K7Pt7z")

##############################################
#######       DATA COLLECT              ######
##############################################

# Para coletar os dados fizemos uso de diversos pacotes do R.
# Detalhes de como usar tais pacotes podem ser encontrados em
# https://rpubs.com/hudsonchavs/coletardados. Note que já temos
# os códigos de cada série temporal de interesse, mas o processo
# de coleta de tais dados envolve uma etapa que é identificar 
# estes códigos. No link anterior, você vai entender como fazer isso.


####
## OPÇÕES PARA A COLETA
####

from = "2002-01-01"
type = "xts"

####
## DADOS NACIONAIS
####

# Câmbio: Dólar americano (venda) - diário (Sisbacen)
cambio = Quandl::Quandl("BCB/1", type = type, start_date = from)
colnames(cambio) = "cambio"

# Taxa referencial de swaps DI pré-fixada - Prazo de 360 dias - diário (BM&F)
swdipre360 = Quandl::Quandl("BCB/7806", type = type, start_date = from)
colnames(swdipre360) = "swdipre360"

# Expedição de caixas, acessórios e chapas de papelão ondulado: quantidade - Mensal (ABPO)
# Usado como antecedente para produção da indústria
#ExpecInd = series_ipeadata(31873)
#ExpecInd = as.data.frame(ExpecInd)
#colnames(ExpecInd)[2] = "ExpecInd"
#colnames(ExpecInd)[1] = "date"

# Sondagem de Serviços - Índice de Expectativas (IE-S) - Dessazonalizado - Em pontos - Mensal (BCB e FGV)
#expecserv = Quandl::Quandl("BCB/20341", type = type, start_date = from)
#colnames(expecserv) = "expecserv"

# Índice de Confiança do Consumidor - Mensal (Fecomércio)
icc = Quandl::Quandl("BCB/4393", type = type, start_date = from)
colnames(icc) = "icc"

# Indicadores da produção (2012=100) - Bens de consumo duráveis - Mensal (IBGE)
ipbcd = BETSget(21866, data.frame = T) %>%
  dplyr::as.tbl() %>%
  dplyr::select(date, ipbcd = value) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "ipbcd")

ipbcd = ipbcd[paste0(from,"/")] # selecionar para o período de interesse

# Termos de Troca TDT=VE/VI
# 2946	Exportações - Total (MDIC) em US$
# 3034	Importações - Total (MDIC) em US$
exp = Quandl::Quandl("BCB/2946", type = type, start_date = from)
imp = Quandl::Quandl("BCB/3034", type = type, start_date = from)
tdt = exp - imp 
colnames(tdt) = "tdt"

# Expectativa IPCA acumulado para os próximos 12 meses (Relatório Focus) - diário e em %
ipca12m = get_twelve_months_inflation_expectations("IPCA") %>% 
  dplyr::filter(smoothed == "N" & base == 0) %>%
  dplyr::select(date, ipca12 = median) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "ipca12")

ipca12m = ipca12m[paste0(from,"/")] # selecionar para o período de interesse

# Expectativa PIB para o ano seguinte  (Relatório Focus) - mensal e em %
pib_expect = get_annual_market_expectations('PIB Total') %>%
  group_by(date) %>%
  mutate(ano = lubridate::year(date)) %>%
  group_by(date) %>%
  mutate(diffyear = as.numeric(reference_year) - ano) %>%
  filter(diffyear == 1) %>%
  dplyr::select(date, pib_expect = median) %>%
  ungroup() %>%
  tbl2xts::tbl_xts(., cols_to_xts = "pib_expect")

pib_expect = pib_expect[paste0(from,"/")] # selecionar para o período de interesse


# EMBI+ Risco-Brasil - diário e em pontos base (JP Morgan) 
embib = series_ipeadata(40940, periodicity = "D") %>%
  as.data.frame() %>%
  dplyr::as.tbl() %>%
  dplyr::select(date = serie_40940.data, embib = serie_40940.valor) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "embib")

embib = embib[paste0(from,"/")] # selecionar para o período de interesse

# Transações Correntes acumulado em 12 meses em relação ao PIB - mensal e em % (BCB)
ccspib12ma = Quandl::Quandl("BCB/23079", type = type, start_date = from)
colnames(ccspib12ma) = "ccspib12ma"

# Investimento Direto no País acumulado em 12 meses em relação ao PIB - mensal e  em % (BCB)
invspib12ma = BETSget(23080, data.frame = T) %>%
  dplyr::as.tbl() %>%
  dplyr::select(date, invspib12ma = value) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "invspib12ma")

invspib12ma = invspib12ma[paste0(from,"/")] # selecionar para o período de interesse

# Dívida Líquida do Setor Público (% PIB) - Total - Setor público consolidado - mensal e em % (BCB)
divpubtotal = Quandl::Quandl("BCB/4513", type = type, start_date = from)
colnames(divpubtotal) = "divpubtotal"

# Reservas internacionais - Total - mensal e em US$ (milhões) (BCB)
reservabba = Quandl::Quandl("BCB/3546", type = type, start_date = from)
colnames(reservabba) = "reservabba"

# Saldo da carteira de crédito em relação ao PIB - mensal e em % (BCB)
credsobpib = Quandl::Quandl("BCB/20622", type = type, start_date = from)
colnames(credsobpib) = "credsobpib"

# Percentual do total de provisões em relação à carteira de crédito das instituições financeiras sob controle público
# Mensal e em % (BCB)
provsobcartbancpub = Quandl::Quandl("BCB/13666", type = type, start_date = from)
colnames(provsobcartbancpub) = "provsobcartbancpub"

# Percentual do total de provisões em relação à carteira de crédito das instituições financeiras sob controle privado
# Mensal e em % (BCB)
provsobcartbancpriv = Quandl::Quandl("BCB/13684", type = type, start_date = from)
colnames(provsobcartbancpriv) = "provsobcartbancpriv"

# PIB acumulado dos últimos 12 meses - Mensal, em valores correntes e deflacionado IPCA (R$ milhões) (BCB)
pibacum12m = Quandl::Quandl("BCB/4382", type = type, start_date = from)
colnames(pibacum12m) = "pibacum12m"
pri = first(index(pibacum12m))
pri = format(pri, format="%Y/%m/%d")
ult = last(index(pibacum12m))
ult = format(ult, format="%Y/%m/%d")
actual_dates <- seq.Date(from = as.Date(pri), to = as.Date(ult), by = "month")
hoje = last(index(pibacum12m))
hoje = format(hoje, format="%m/%Y")
pibacum12_real =  ipca(pibacum12m, actual_dates, hoje)

# Inadimplência da carteira de crédito das instituições financeiras sob controle privado - Total - Em % e mensal (BCB)
default = Quandl::Quandl("BCB/13685", type = type, start_date = from)
colnames(default) =  "default"

# Indicadores da produção (2012=100) - Indústria de transformação - Índice  mensal (IBGE)
indtransf = BETSget(21862, data.frame = T) %>%
  dplyr::as.tbl() %>%
  dplyr::select(date, indtransf = value) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "indtransf")

indtransf = indtransf[paste0(from,"/")] # selecionar para o período de interesse

# Juros real ‘ex-ante’ calculado a partir do Swap Pré de 360 dias descontada a projeção mediana de inflação de doze meses à frente (Fonte BC e FOCUS)
tjex_ante = swdipre360 - ipca12m
colnames(tjex_ante) = "tjex_ante"

# Inclinação da parte curta da curva de juros CDI menos SWAP pré 3 meses (FONTE: Bacen)
selicmt = Quandl::Quandl("BCB/432", type = type, start_date = from)        # taxa de juros selic meta diária em valor % (a.a.) 
colnames(selicmt) = "selicmt"

swdipre90 = Quandl::Quandl("BCB/7803", type = type, start_date = from)     # taxa swaps DI pré-fixada 90 dias diária em valor % (a.a)
colnames(swdipre90) = "swdipre90"

cdia = Quandl::Quandl("BCB/4389", type = type, start_date = from)          # taxa de juros CDI anualizada em valor % (a.a.)
colnames(cdia) = "cdia"

incl_cj = cdia - swdipre90
colnames(incl_cj) = "incl_cj"

# Índice Bovespa
bvsp = quantmod::getSymbols("^BVSP", src = "yahoo", auto.assign = FALSE, 
                            from = from, return.class = type) %>% na.omit()

bvsp = bvsp$BVSP.Close

# Volatilidade Mensal do Índice Bovespa 
rv = bvsp$BVSP.Close %>%
  tbl2xts::xts_tbl() %>%
  dplyr::mutate(return = (BVSP.Close/xts::lag.xts(BVSP.Close)-1)*100) %>%
  dplyr::mutate(mes = lubridate::month(date)) %>%
  dplyr::mutate(ano = lubridate::year(date)) %>%
  dplyr::mutate(sqad_return = return^2) %>%
  dplyr::group_by(ano, mes) %>%
  dplyr::summarise(rv = sum(sqad_return)) %>%
  dplyr::mutate(data = as.Date(zoo::as.yearmon(paste(ano,mes), "%Y %m"))) %>%
  dplyr::ungroup() %>%
  dplyr::select(data, rv) %>%
  dplyr::ungroup()

rv = data.frame(rv$data, rv$rv)
rv <- xts(rv[,-1], order.by=rv[,1])
colnames(rv) = "rv"

####
## DADOS INTERNACIONAIS
####

ust10 = Quandl::Quandl("FRED/DGS10", type = type, start_date = from)
colnames(ust10) = "ust10"

vixcls = tidyquant::tq_get("VIXCLS", get = "economic.data", from = from) %>%
  dplyr::select(date, vixcls = price) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "vixcls")

vixcls = vixcls[paste0(from,"/")] # selecionar para o período de interesse

rici = Quandl("RICI/RICI", type = type, start_date = from)
colnames(rici) = "rici"

t10y3m = tidyquant::tq_get("T10Y3M", get = "economic.data", from = from) %>%
  dplyr::select(date, t10y3m = price) %>%
  tbl2xts::tbl_xts(., cols_to_xts = "t10y3m")

t10y3m = t10y3m[paste0(from,"/")] # selecionar para o período de interesse

##############################################
#######    EXPLORATORY DATA ANALYSIS   #######
##############################################

# Séries temporais diárias
daily = xts::merge.xts(cambio, swdipre360, ipca12m, embib, tjex_ante, incl_cj,
                       bvsp, ust10, vixcls, rici, t10y3m, pib_expect)

daily_names = colnames(daily)
saveRDS(daily,   file = "/cloud/project/data/daily.rds")

# Séries temporais mensais
monthly = xts::merge.xts(icc, ipbcd, tdt, ccspib12ma, invspib12ma,
                         divpubtotal, reservabba, credsobpib, provsobcartbancpub,
                         provsobcartbancpriv, pibacum12_real, default, indtransf, rv)

monthly_names = colnames(monthly)
saveRDS(monthly,   file = "/cloud/project/data/monthly.rds")

# Gráfico das séries temporais - agrupado com 4 séries por tipo
setwd("/cloud/project/plot")
g_cambio = autoplot(cambio) + 
  labs(title="Câmbio em R$/US$",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_swdipre360 = autoplot(swdipre360) + 
  labs(title="Swap DI pré-fixada 360 Dias",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_indtransf = autoplot(indtransf) + 
  labs(title="Indicadores da produção - Indústria de transformação",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_icc = autoplot(icc) + 
  labs(title="Índice de Confiança do Consumidor",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_cambio, g_swdipre360, g_indtransf, g_icc,
                        nrow = 2, ncol = 2) %>%
  ggexport(filename = "1.png",
           width = 1200, height = 700)

g_ipca12m = autoplot(ipca12m) + 
  labs(title="Projeção do IPCA 12 meses",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_embib = autoplot(embib) + 
  labs(title="Risco Brasil",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_ipbcd = autoplot(ipbcd) + 
  labs(title="Indicador de produção - Bens de consumo duráveis",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_tdt = autoplot(tdt) + 
  labs(title="Termos de Troca",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_ipca12m, g_embib, g_ipbcd, g_tdt,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "2.png",
           width = 1200, height = 700)

g_tjex_ante = autoplot(tjex_ante) + 
  labs(title="Juros real ‘ex-ante’",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_incl_cj = autoplot(incl_cj) + 
  labs(title="Inclinação da taxa de juros",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_ccspib12ma = autoplot(ccspib12ma) + 
  labs(title="TC acumulado em 12 meses em relação ao PIB",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_invspib12ma = autoplot(invspib12ma) + 
  labs(title="CF acumulado em 12 meses em relação ao PIB",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_tjex_ante, g_incl_cj, g_ccspib12ma, g_invspib12ma,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "3.png",
           width = 1200, height = 700)

g_bvsp = autoplot(bvsp) + 
  labs(title="Bovespa",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_ust10 = autoplot(ust10) + 
  labs(title="Título norte americano de dez anos",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_divpubtotal = autoplot(divpubtotal) + 
  labs(title="Dívida Líquida do Setor Público (% PIB)",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_reservabba = autoplot(reservabba) + 
  labs(title="Reservas internacionais",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_bvsp, g_ust10, g_divpubtotal, g_reservabba,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "4.png",
           width = 1200, height = 700)

g_vixcls = autoplot(vixcls) + 
  labs(title="VIX",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_rici = autoplot(rici) + 
  labs(title="RICI",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_credsobpib = autoplot(credsobpib) + 
  labs(title="Carteira de crédito do banco privado em relação ao PIB",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_provsobcartbancpub = autoplot(provsobcartbancpub) + 
  labs(title="Provisões da carteira de crédito dos bancos públicos",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_vixcls, g_rici, g_credsobpib, g_provsobcartbancpub,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "5.png",
           width = 1200, height = 700)

g_t10y3m = autoplot(t10y3m) + 
  labs(title="Inclinação da curva de juros do EUA",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_provsobcartbancpriv = autoplot(provsobcartbancpriv) + 
  labs(title="Provisões dos bancos privados",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Diário') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_pibacum12m = autoplot(pibacum12_real) + 
  labs(title="PIB real acumulado dos últimos 12 meses",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_default = autoplot(default) + 
  labs(title="Inadimplência da carteira de crédito dos bancos privados",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_t10y3m, g_provsobcartbancpriv, g_pibacum12m, g_default,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "6.png",
           width = 1200, height = 700)

g_rv = autoplot(rv) + 
  labs(title="Volatilidade Mensal",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
g_pib_expect = autoplot(pib_expect) + 
  labs(title="Expectativa do PIB para o próximo ano",
       caption = "Fonte: Elaboração própria",
       subtitle = 'Mensal') + 
  scale_x_date(breaks = date_breaks("2 years"),
               labels = date_format("%Y"))+
  theme(plot.title = element_text(hjust=0.05))
ggarrange(g_rv, g_pib_expect,
          nrow = 2, ncol = 2) %>%
  ggexport(filename = "7.png",
           width = 1200, height = 700)

# Estatística descritiva das séries 
statNames = c("Média", "Desvio Padrão", "Assimetria", "Curtose", "Tamanho")
series_stats_daily = matrix(data = NA, nrow = length(daily_names), ncol = length(statNames))
colnames(series_stats_daily) = statNames
series_stats_monthly = matrix(data = NA, nrow = length(monthly_names), ncol = length(statNames))
colnames(series_stats_monthly) = statNames


for (i in 1:nrow(series_stats_daily)) {
  series_stats_daily[i,1] = round(mean(daily[,i], na.rm = TRUE),3)
  series_stats_daily[i,2] = round(sd(daily[,i], na.rm = TRUE), 3)
  series_stats_daily[i,3] = round(skewness(daily[,i], na.rm = TRUE), 3)
  series_stats_daily[i,4] = round(kurtosis(daily[,i], na.rm = TRUE), 3)
  series_stats_daily[i,5] = length(na.omit(daily[,i]))
}

for (i in 1:nrow(series_stats_monthly)){
  series_stats_monthly[i,1] = round(mean(monthly[,i], na.rm = TRUE), 3)
  series_stats_monthly[i,2] = round(sd(monthly[,i], na.rm = TRUE), 3)
  series_stats_monthly[i,3] = round(skewness(monthly[,i], na.rm = TRUE), 3)
  series_stats_monthly[i,4] = round(kurtosis(monthly[,i], na.rm = TRUE), 3)
  series_stats_monthly[i,5] = length(na.omit(monthly[,i]))
}

rownames(series_stats_daily) = daily_names
rownames(series_stats_monthly) = monthly_names
series_stats = rbind(series_stats_daily, series_stats_monthly)
series_stats = as.data.frame(series_stats)
png("tabela.png", width = 700, height = 700)
ggtexttable(series_stats) +
  theme_void()+
  labs(title = "Dados estatísticos",
       caption = "Fonte: Elaboração própria")
dev.off()

