pib_expect = get_annual_market_expectations('PIB Total') %>%
  group_by(date) %>%
  mutate(ano = lubridate::year(date)) %>%
  group_by(date) %>%
  mutate(diffyear = as.numeric(reference_year) - ano) %>%
  filter(diffyear == 1) %>%
  dplyr::select(date, pib_expect = median) %>%
  ungroup() %>%
  tbl2xts::tbl_xts(., cols_to_xts = "pib_expect")
