Definição das séries temporais

- Câmbio: diário de 28/11/1984 a 26/10/2018. Câmbio foi utilizado ao longo da história como instrumento de política do governo, onde vários governos utilizavam desse instrumento para reduzir inflação. O câmbio tende a oscilar bastante frente a frustrações do cenário macro brasileiro e também quando ocorre aumento de risco de instabilidade do cenário internacional. Essa série é importante pois incorpora dados macro e financeiros de relação de troca de bens e ativos, sendo utilizado em modelos financeiros como carry trade.

- Swaps DI pré-fixada: diário de 20/08/1999 a 25/10/2018. Essa série foi utilizada para cálculos de outras.

- Expedição de caixas, papelão: mensal de 01/01/1980 a 01/09/2018. Essa série corresponde a um indicador do qual podemos extrair a informação de solicitação das indústras de transformação por papelão para que seja embalado e enviado o seu produto, ou seja, é usado como antecedente para atividade da indústria de transformação

- Sondagem de serviços: mensal de 01/06/2008 a 01/07/2018. Expectativa do setor de serviços, reflete o volume de demanda atual, situação atual dos negócios e expectativas sobre volume de demanda (três meses) e Situação dos negócios (seis meses).

- Índice de Confiança do Consumidor: mensal de 01/03/1999 a 01/08/2018. É um indice mensal que procura medir o sentimento do consumidor em relação a finanças pessoais e estado geral da economia do indivíduo. 

- Índice de produção: mensal de 01/01/2002 a 01/08/2018. Indice de produção industrial no Brasil, reflete a capacidade das indústrias em produção. Por mais que produção não seja um setor alto como serviço no Ibovespa, reflete parte da saúde da economia brasileira.
# n precisa
- Termos de troca: mensal de 01/01/2000 a 01/09/2018. 
# n precisa

- IPCA Expectativa para os próximos 12 meses: a gente precisa entender bem o resultado dos dados que você pegou,
principalmente aquela data. Trata-se da data que a projeção foi feita para os próximos 12 mses ou a projeção foi 
feita há 12 meses atras para esta data? Acredito que seja o primeiro caso, mas precisamos confirmar. De 02/01/2014 a 19/10/2018
# Resp: primeiro caso
# Alguns artigos citam que inflação descontrolada prejudica retornos no mercado financeiro, outros já citam que não existe correlação nem causaliade entre inflação e retorno no mercado financeiro. 

- EMBI+ Risco Brasil: O EMBI+ é um índice baseado nos bônus (títulos de dívida) emitidos pelos países emergentes. Mostra os retornos financeiros obtidos a cada dia por uma carteira selecionada de títulos desses países. A unidade de medida é o ponto-base. Dez pontos-base equivalem a um décimo de 1%.Os pontos mostram a diferença entre a taxa de retorno dos títulos de países emergentes e a oferecida por títulos emitidos pelo Tesouro americano. Essa diferença é o spread, ou o spread soberano. O EMBI+ foi criado para classificar somente países que apresentassem alto nível de risco segundo as agências de rating e que tivessem emitido títulos de valor mínimo de US$ 500 milhões, com prazo de ao menos 2,5 anos: diário de 29/04/1994 a 24/10/2018. Esse indicador reflete o interesse dos investidores estrangeiros em títulos brasileiros, quanto maior o spread, maior será a percepção de risco dos investires sobre o Brasil, logo um retorno maior dado um risco maior torna-se necessário.

- Transações Correntes (%): A relação transações correntes/PIB, acumulados em 12 meses (%), permite ponderar o resultado de transações correntes na medida em que considera o tamanho da economia. Todas as transações entre os residentes no Brasil e os residentes no exterior, independentemente da moeda utilizada, realizadas por intermédio do sistema bancário ou de agentes credenciados pelo Banco Central do Brasil. Transações corrente pode ser um modo de financiar deficits em conta financeira sem a redução de reservas internacionais, países que tem problema em financiar seus déficits de BP, costumam ter alta desvalorização cambial e pedindo ajuda de orgãos mundiais para terem dolares o suficiente para saldar deficits em balança de pagamento. Momentos assim geram uma grande oscilação no mercado financeiro dado a incerteza de acordos que os orgãos mundiais irão pedir para consederem os empréstimos de dolares.
# n precisa

- Investimento direto no país: Registra os fluxos financeiros de passivos emitidos por residentes brasileiros para credores não residentes, nos quais os agentes institucionais possuem uma relação de controle ou forte poder de influência entre si. A relação investimento direto no país/PIB, acumulados em 12 meses (%), permite ponderar o resultado de investimento direto no país na medida em que considera o tamanho da economia. De 01/01/1995 a 01/09/2018. Valores possitivos no indicador podem sugerir que investidores estrangeiros estão alocando em seus portfolios ativos brasileiros, vale o oposto para valores negativos no indicador. Como quase que metade do fluxo da bolsa brasileira vem de fora, acompanhar esse indicador seja importante
# n precisa

- Reservas Internacionais: As reservas internacionais compreendem os ativos externos do país prontamente disponíveis, mantidos pelo Banco Central do Brasil com o objetivo de financiar eventuais déficits do balanço de pagamentos. De 01/01/1980 a 01/09/2018. Reservas internacionais relativamente superior aos outros emergentes, deixa o Brasil menos frágil a choques externos.
# n precisa

- Dívida Pública: O conceito de setor público utilizado para mensuração é o de setor público não-financeiro mais Banco Central. Considera-se como setor público não financeiro as administrações diretas federal, estaduais e municipais, as administrações indiretas, o sistema público de previdência social e as empresas estatais não-financeiras federais, estaduais e municipais, exceto as empresas do Grupo Petrobras e do Grupo Eletrobras. Considera-se também a empresa Itaipu Binacional. A exclusão da Petrobras e da Eletrobras deve-se às características específicas das empresas, seguidoras de regras de governança corporativa similares às experimentadas pelas empresas privadas de capital aberto, e com autonomia para captar recursos nos mercados interno e externo. A exclusão da Petrobras das estatísticas fiscais passou a ser realizada a partir dos dados referentes ao mês de maio de 2009. De 01/12/2001 a 01/08/2018. Dívida externa elevada em comparação aos demais países emergentes deixa o país mais vulnerável a choques externos, também causam um aumento da taxa de juros básico da economica, dado que o país tem sido uns dos maiores demandantes de crédito. 
# n precisa

- Saldo Carteira de Crédito: Relação percentual entre o saldo de crédito concedido pelo Sistema Financeiro Nacional e o valor do PIB acumulado nos últimos doze meses a valores correntes. Saldo: corresponde ao somatório do saldo devedor dos contratos de crédito em final de mês. Inclui as novas concessões liberadas no mês de referência e a apropriação de juros pró-rata dos empréstimos e financiamentos. De 01/07/1995 a 01/09/2018. Bancos começam a ofertar mais crédito quando sentem que a economia não sofrerá choques negativos nos próximos meses, Banco de maneira geral tem contato direto com empresários
# n precisa

- Provisões em relação à carteira: Os dados referem-se às operações de crédito realizadas pelas instituições financeiras no Brasil (não inclui dados de agências e subsidiárias de bancos brasileiros no exterior), com devedores predominantemente domiciliados no país. Algumas operações específicas são pactuadas com devedores domiciliados no exterior. Instituições financeiras abrangidas: associação de poupança e empréstimo, bancos comerciais, bancos de câmbio, bancos de desenvolvimento, bancos de investimento, bancos múltiplos, caixas econômicas, companhias hipotecárias, sociedades de arrendamento mercantil, sociedades de crédito, financiamento e investimento e sociedades de crédito imobiliário.
    * Sistema financeiro público: operações realizadas por instituições financeiras em que a União e os governos estaduais detêm a maioria do capital votante, de forma direta ou indireta. De 01/06/1988 e 01/09/2018
    * Sistema financeiro privado nacional: operações realizadas por instituições financeiras em que pessoas físicas e/ou jurídicas domiciliadas e residentes no País detém a maioria do capital votante. De 01/06/1988 a 01/09/2018
# n precisa
    
- PIB acumulado últimos 12 meses: Você calculou a diferença para obter a taxa de crescimento do PIB mensal? Você não acha que seria melhor usar o IBC-Br mensal com ajuste sazonal do BACEN? Além disso, o PIB em valores correntes não elimina a inflação e a taxa de crescimento que você vai obter mensalmente será enviesada. Acho que se formos continuar com o PIB temos que deflacionar. De 01/01/1990 a 01/09/2018. O PIB mensal é estimado com base em informações divulgadas da produção da indústria de transformação, do consumo de energia elétrica, da exportação de produtos primários e de índices de preços. Inicialmente se obtém a estimativa do índice em volume a partir das três primeiras séries citadas. Essas estimativas são ajustadas aos dados de volume do PIB trimestral divulgado pelo IBGE, ou à projeção do Banco Central para o PIB real quando as Contas Nacionais Trimestrais referentes ao período mais recente ainda não foram divulgadas pelo IBGE.  EM seguida, para obter a estimativa do PIB nominal, o índice mensal de volume é inflacionado por uma média ponderada do IGP-DI e do IPCA. O IBC-Br foi construído com base em proxies da evolução em volume dos produtos da agropecuária, indústria e serviços. As proxies são agregadas de acordo com pesos derivados das tabelas de recursos e usos do Sistema de Contas Nacionais.
# Concordo, iremos utlizar o IBC-BR, colocar expectativa do PIB

- Inadimplência: Refere-se ao saldo das operações de empréstimo, financiamento, adiantamento e arrendamento mercantil concedidas pelas instituições integrantes dos sistemas financeiros privado nacional e estrangeiro. De 01/03/2000 a 01/09/2018.
   * Sistema financeiro privado nacional: operações realizadas por instituições financeiras em que pessoas físicas e/ou jurídicas domiciliadas e residentes no País detém a maioria do capital votante
# n precisa
   
- Índice da indústria de transformação: De 01/01/2002 a 01/08/2018. É um indicador de produção industrial, o índice compara a produção do mês de referência do índice com a média mensal produzida no ano base da pesquisa (2012). Cujo as empresas representam aproximadamente 85% do Valor da Transformação Industrial da PIA-Empresa do ano de 2010, abrangendo 944 produtos e 7.800 unidades locais a partir de janeiro de 2012.
# n precisa

- Juro real ex-ante: A taxa de juros futuro ex-ante é calculada descontando o SWAP DI PRÉ fixado de 360 dias da expectativa da mediana do IPCA dozes meses a frente. A taxa é utilizada em modelos de desconto de fluxo de caixa futuro em projetos, em modelos de precificação de ativos listados em bolsa e preferências dos consumidores entre poupar e consumir. De 02/01/2014 a 26/10/2018.	

- Inclinação da parte curta da curva de juros. Calculado através do desconto do CDI anualizada na base de 252 dias úteis menos Swap DI pré fixado 3 meses. Essa taxa tende a oscilar mais em momentos de inversão de expectativas quanto à política monetária, normalmente em função de choques. De 20/08/1999	a 26/10/2018.

- Taxa de juros norte americana de dez anos

- VIX 

- EUCI

- Taxa de juros norte americana de dez anos menos a taxa de juros de tres meses