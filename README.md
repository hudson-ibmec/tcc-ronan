# **README**
Neste repositório você encontrará informações e códigos necessários para replicar o Trabalho de Conclusão de Curso (TCC) do aluno Ronan Alexandre. Tal TCC está em formato de artigo conforme exigência do Ibmec - MG para o curso de Ciências Econômicas.  

### **RESUMO**
Despois, vamos adicionar o resumo do seu artigo.

### **OBJETIVOS**
Depois, vamos adicionar os objetivos do seu artigo.

### **PRÉ-REQUISITOS**
O artigo foi gerado no [RStudio Cloud](https://rstudio.cloud/) por meio de um projeto que está disponível neste link. Nele você encontrará todos os códigos necessários para gerar o PDF do artigo bem como fazer as análises. Outra alternativa é clonar este repositório e executar o código em seu computador/notebook pessoal.

Para conseguir entender os códigos e replicar o artigo recomendamos que você faça os seguintes cursos na plataforma [DataCamp](https://www.datacamp.com/):

* [Working with the RStudio IDE (Part 1)](https://www.datacamp.com/courses/working-with-the-rstudio-ide-part-1)
* [Working with the RStudio IDE (Part 2)](https://www.datacamp.com/courses/working-with-the-rstudio-ide-part-2)
* [Introduction to R](https://www.datacamp.com/courses/free-introduction-to-r)
* [Intermediate R](https://www.datacamp.com/courses/intermediate-r)
* [Introduction to Shell for Data Sciente](https://www.datacamp.com/courses/introduction-to-shell-for-data-science)
* [Introduction to Git for Data Sciente](https://www.datacamp.com/courses/introduction-to-git-for-data-science)

### **ESTRUTURA DE PASTAS DESTE REPOSITÓRIO**

Para facilitar o entendimento, dividimos os arquivos em:

* `code`: pasta com os códigos das análises
* `data`: pasta com os dados usados ou gerados durante a elaboração do artigo
* `references`: pasta com artigos usados como base ou citados no artigo
* `paper`: pasta com código que gera o artigo em formato PDF
